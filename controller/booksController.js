//adding functions for get and post, so that it becomes easier for testing

function booksController(Book) {
    function post(req, res) {
        const book = new Book(req.body);
        //mapping of ORM with the schema
        book.save();
        //saving it to database
        console.log(book);
        res.status(201);
        return res.json(book)
    }
    function get(req, res) {
        const query = req.query;
        Book.find(query, (err, books) => {
            if (err) {
                return res.send(err);
            }
            return res.json(books);
        });
    }
    return { post, get };

    }
module.exports = booksController;