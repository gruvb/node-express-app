const express = require('express')
const path = require('path');
const app = express();
const mongoose = require('mongoose');
// const bookRouter = express.Router();
const bodyParser = require('body-parser');
//Database Connection
const db = mongoose.connect('mongodb://localhost/bookAPI');
const Book = require('./models/bookModel');
const bookRouter = require('./routes/bookRouter')(Book);
//Book(mongoose model) inserted as a parameter for bookRouter

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.use('/', bookRouter);
// console.log(__dirname)
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'views/home.html'))
    // res.send('Hello World');
});


app.listen(3000, function() {
    console.log('Listening on 3000');}
); 