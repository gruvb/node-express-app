const express = require('express');
const booksController = require('../controller/booksController')
function routes(Book){

const bookRouter = express.Router();

const controller = booksController(Book);

bookRouter.route('/books')
.post(controller.post)
.get(controller.get)
// const response = {hello: 'This is my API'};

// res.json(response)


bookRouter.route('/books/:bookId')
.get((req, res) => {
// const book = new Book(req.body);
// const query = req.query;
//mongoose has a method-findbyid
Book.findById(req.params.bookId, (err, book) => {
    if (err) {
        return res.send(err);
    }
    return res.json(book)
})
// const response = {hello: 'This is my API'};

// res.json(response)
})
.put((req, res) => {
    Book.findById(req.params.bookId, (err, book) => {
        if (err) {
            return res.send(err);
        }
        book.title = req.body.title;
        book.author = req.body.author;
        book.genre = req.body.genre;
        book.read = req.body.read;
        book.save();
        return res.json(book)
    })
})
.patch((req, res) => {
    //implementing patching without middleware
})
.delete((req, res) => {
    //get the id and delete
    Book.findById(req.params.bookId, (err, book) => {
        if (err) {
            return res.send(err);
        }
        book.delete();
        return res.sendStatus(204);
    })
})
//implement patch and delete
return bookRouter;

}


module.exports = routes;